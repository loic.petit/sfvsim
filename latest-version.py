import os

ignore_chars = {"NDK", "B00", }

root = "D:\\Steam\\SteamApps\\common\\StreetFighterV\\StreetFighterV\\Content\\Paks\\moves\\StreetFighterV\\Content\\Chara"
for dir in os.listdir(root):
    char_root = root + "\\" + dir + "\\BattleResource"
    if not os.path.exists(char_root) or dir in ignore_chars:
        continue
    # print(dir)
    all_versions = sorted([v for v in os.listdir(char_root) if int(v[0]) < 5])
    if not all_versions:
        continue
    latest_version = all_versions[-1]
    print(dir, latest_version)
