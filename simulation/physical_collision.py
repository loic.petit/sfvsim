from sfvfile import Script
from typing import Tuple

from model import ZERO, FLT, TWO, PlayerState, PositionShiftsConstants
from .log import position_logger

BOUND = FLT(7.5)


def apply_physical_collision(p1: PlayerState, p2: PlayerState, script_p1: Script, script_p2: Script, old_side: bool):
    """
    Apply physical collision (between players push box and corners)

    :param p1: p1 state
    :param p2: p2 state
    :param script_p1: script played by p1
    :param script_p2: script played by p2
    """
    pos_before_correction = p1.pos.coord[0], p2.pos.coord[0]
    # Corner correction now
    corner_correction(p1)
    corner_correction(p2)

    # Decide which side are actually the characters
    if p1.pos.coord[0] == p2.pos.coord[0]:
        # this is still faulty but good for now
        # https://www.youtube.com/watch?v=6vEUyiNElPI
        new_x1 = pos_before_correction[0] + FLT(1.0 if p1.pos.side else -1.0) * (p1.pos.speed[0] + p1.pos.shift[0] - (script_p1.get_position_shift_at(p1.script_state, PositionShiftsConstants.X, p1.script.time-1) or ZERO))
        new_x2 = pos_before_correction[1] - FLT(1.0 if p2.pos.side else -1.0) * (p2.pos.speed[0] + p2.pos.shift[0] - (script_p2.get_position_shift_at(p2.script_state, PositionShiftsConstants.X, p2.script.time-1) or ZERO))
        side = new_x1 < new_x2 if new_x1 != new_x2 else p1.pos.side
    else:
        side = old_side

    # #204 during a throw, pushbox collision is not applied (check out NCL GAIA_H)
    # Unless the lock transition is disabled (end of Ken's THROW_F)
    if p2.lock_transition or p1.lock_transition:
        return

    expulsion = ZERO
    was_full_correction = False
    for h in p1.phys:
        for h2 in p2.phys:
            if h.does_collide(h2):
                distance = h.x_collision_distance(h2, side)
                if distance > expulsion:
                    was_full_correction = side != (h.x <= h2.x)
                    expulsion = distance
    if expulsion > 0:
        dx1 = ZERO
        dx2 = ZERO
        vx = FLT(1.0 if side else -1.0) * expulsion
        # Opponent's recovering so (s)he must take the repulsion
        if 50 <= p2.script.script < 400 and (p2.stun is None or p2.stun == 0):
            dx2 += vx
        elif 50 <= p1.script.script < 400 and (p1.stun is None or p1.stun == 0):
            dx1 -= vx
        else:
            dx2 += vx / TWO
            dx1 -= vx / TWO

        # Corner correction now
        min_x = min(p1.pos.coord[0] + dx1, p2.pos.coord[0] + dx2)
        max_x = max(p1.pos.coord[0] + dx1, p2.pos.coord[0] + dx2)
        if min_x < -BOUND:
            dx1 -= min_x + BOUND
            dx2 -= min_x + BOUND
        elif max_x > BOUND:
            dx1 -= max_x - BOUND
            dx2 -= max_x - BOUND

        # BUGSFV
        # When trying to correct an equality, SFV moves the position a smudge
        # This introduces a stupid offset (really stupid)
        # The condition on the full correction seems not good...
        if p1.pos.coord[0] == p2.pos.coord[0] and not was_full_correction:
            if side:
                dx1, dx2 = get_post_corner_equal_shift_bug(p1, script_p1, dx1, dx2)
            else:
                dx2, dx1 = get_post_corner_equal_shift_bug(p2, script_p2, dx2, dx1)
            position_logger.debug("Trying to correct %f %f", dx1, dx2)

        # Shift everything
        p1.pos.coord[0] += dx1
        p1.pos.ref[0] += dx1
        for h in p1.phys:
            h.apply_correction(dx1)
        p2.pos.coord[0] += dx2
        p2.pos.ref[0] += dx2
        for h in p2.phys:
            h.apply_correction(dx2)


def corner_correction(p: PlayerState):
    """
    If a player steps out of bound, correct the positions

    Can modify opponent position as well

    :param p: the player
    """
    if p.pos.coord[0] < -BOUND:
        correction = p.pos.coord[0] + BOUND
    elif p.pos.coord[0] > BOUND:
        correction = p.pos.coord[0] - BOUND
    else:
        return

    p.pos.coord[0] -= correction
    p.pos.ref[0] -= correction
    for h in p.phys:
        h.apply_correction(-correction)
    # the correction must be applied to the opponent as well
    if (not p.status_dont_push and not p.opponent.status_dont_push) or p.lock_transition:
        p.opponent.pos.coord[0] -= correction
        p.opponent.pos.ref[0] -= correction
        for h in p.opponent.phys:
            h.apply_correction(-correction)


def get_post_corner_equal_shift_bug(p: PlayerState, script: Script, dx: FLT, dx_op: FLT) -> Tuple[FLT, FLT]:
    """
    Compute the corner shift that players have (see comment in the calling section)

    :param p: a player state
    :param script: the player script
    :param dx: the expulsion computed for this player
    :param dx_op: the expulsion computed for the opponent
    :return: the new (dx, dx_op) after applying the bug
    """
    vx = p.pos.speed[0]
    if p.stun_force is not None:
        vx += p.stun_force.speed[0]
    if vx != 0:
        dx -= vx
        if p.opponent.pos.speed[0] == ZERO:
            dx_op -= vx
    else:
        shift_dx = p.pos.shift[0] - (script.get_position_shift_at(p.script_state, PositionShiftsConstants.X, p.script.time - 1) or ZERO)
        dx -= shift_dx
        if p.opponent.pos.speed[0] == ZERO:
            dx_op -= shift_dx
    return dx, dx_op

