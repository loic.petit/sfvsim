from sfvfile import Script, Hitbox
from typing import List

from model import ZERO, PlayerState, PositionShiftsConstants
from .log import hit_logger


def compute_physical_box(p: PlayerState, script: Script):
    time = p.script.time
    x_shift = script.get_position_shift_at(p.script_state, PositionShiftsConstants.X | PositionShiftsConstants.HITBOX_SHIFT, time) or ZERO
    y_shift = script.get_position_shift_at(p.script_state, PositionShiftsConstants.Y | PositionShiftsConstants.HITBOX_SHIFT, time) or ZERO
    x, y = p.pos.coord
    side = p.pos.side
    y += y_shift
    if side:
        x += x_shift
    else:
        x -= x_shift
    p.phys = script.get_pushboxes_at(time, p.script_state, x, y, side)
    return


def filter_hitboxes(p: PlayerState, all_boxes: List[Hitbox]) -> List[Hitbox]:
    boxes = []
    for h in all_boxes:
        # If it is a proximity box, just ignore it
        if h.hit_type == 4 or h.hit_type > 5:
            continue

        # If it is a reset box
        if h.group == 255 and p.effects is not None:
            if p.stun is not None:
                continue
            # If we have used the hitbox before, remove it
            if h.group in p.hit_state.used_hitboxes:
                p.hit_state.used_hitboxes.remove(h.group)

            if p.script.time == h.tick_start:
                # On the first frame, remove the hit box
                if h.group in p.hit_state.hit_hitboxes:
                    p.hit_state.hit_hitboxes.remove(h.group)
            elif h.group in p.hit_state.hit_hitboxes:
                # On other frames, dont show it if we have it
                continue
        else:
            # If we have used the hitbox, get out
            if h.group in p.hit_state.used_hitboxes:
                continue

        if p.opponent.juggle_state == "RESET":
            hit_logger.debug("Ignoring box due to juggle reset")
            continue
        if p.opponent.juggle_state == "JUGGLE" and h.juggle_limit < p.opponent.juggle:
            hit_logger.debug("Ignoring box due to juggle limit=%d < juggle=%d", h.juggle_limit, p.opponent.juggle)
            continue

        boxes.append(h)
    return boxes
