from tests.util import SimulationRun


def test_lar_vs2():
    run = SimulationRun("5.001", "LAR", "RYU", "lar-vs2-hp")
    run.execute("VS2_JUMP", 19)
    run.execute_and_check("VS2_P_H")