from sfvfile import BAC, BCH, BCM, HitboxEffect, Move
from typing import Optional, Tuple, Dict

from model.cancel import get_reserved_scripts
from .cancel import *
from .simulator_state import *
from .utils import *


class CharacterData:
    def __init__(
            self,
            bac: BAC,
            bac_eff: BAC,
            bch: BCH,
            bcm: BCM
    ):
        self.bac_eff = bac_eff
        self.bch = bch
        self.moves = bcm.moves
        self.cancel_lists = bcm.cancel_lists
        self.hitbox_effects = bac.hitbox_effects
        self.script_tables = bac.script_tables
        self.eff_script_tables = bac_eff.script_tables if bac_eff else None
        self.eff_hitbox_effects = bac_eff.hitbox_effects if bac_eff else None
        self.move_idx = {m.name: (m.index, m.script_index) for m in self.moves if m is not None}


class SFVData:
    def __init__(self, chars: Dict[str, CharacterData]):
        self.chars = chars

    def get_current_script(self, p: PlayerState, script_id=None) -> Script:
        char_data = self.chars[p.current_char()]
        if script_id is None or script_id < 0:
            script_id = p.script.script
            if p.effects is None:
                return char_data.eff_script_tables[0].scripts[script_id]
        s = None
        if p.current_stance() != 0 and len(char_data.script_tables) > 1:
            s = char_data.script_tables[1].scripts[script_id]
        if s is None:
            s = char_data.script_tables[0].scripts[script_id]
        if s is None:
            return self.chars["CMN"].script_tables[0].scripts[script_id]
        return s

    def get_hitbox_effect(self, char:str, effect_index: int, effect_type: int, position: int, is_effect=False) -> HitboxEffect:
        char_data = self.chars[char]
        table = char_data.eff_hitbox_effects[effect_index] if is_effect else char_data.hitbox_effects[effect_index]
        return table[effect_type*5 + position]

    def get_cancels(self, char, script_state, script: Script, tick: int) -> Set[int]:
        if script is None:
            return set()
        cancels = set()
        cancel_lists = get_cancel_lists(char, script_state, script, tick)
        char_data = self.chars[char]
        for cl in cancel_lists:
            cld = char_data.cancel_lists[cl]
            if cld is None:
                continue
            for c in cld.moves:
                cancels.add(c.move_id)
        return cancels

    def get_scripts_by_move_name(self, char, move_name) -> Tuple[List[int], Optional[Move]]:
        scripts = get_reserved_scripts(char, move_name)
        if scripts is not None:
            return scripts, None
        move_description = self.chars[char].move_idx[move_name]
        return [move_description[1]], self.chars[char].moves[move_description[0]]

    def get_current_move_description(self, p: PlayerState) -> Optional[Move]:
        if p.move_id is None:
            return None
        return self.chars[p.char].moves[p.move_id]
