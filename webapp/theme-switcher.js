var lightTheme = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css";
var darkTheme = "https://stackpath.bootstrapcdn.com/bootswatch/3.3.7/slate/bootstrap.min.css";
function isDark() {
    return localStorage.getItem("dark") === "1";
}
function toggleLight() {
    localStorage.setItem("dark", isDark() ? "0" : "1");
    location.reload();
    return false;
}
$("#theme-switch").replaceWith('<link id="bootstrap-theme" rel="stylesheet" href="'+(isDark() ? darkTheme : lightTheme)+'">');

$(document).ready(function () {
    if (isDark()) {
        $("body").addClass("dark-mode");
    }
});
